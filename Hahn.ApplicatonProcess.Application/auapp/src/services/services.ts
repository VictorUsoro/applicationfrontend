import { HttpClient } from "aurelia-http-client";
import { inject } from "aurelia-framework";

import api from '../config/api';

@inject(HttpClient)
export class ContactService {
  private http: HttpClient;
  applicants = [];

  constructor(http: HttpClient) {
    http.configure(x => x.withBaseUrl(api.dev + '/contacts/'));
    this.http = http;
  }

  getApplicants() {
    let promise = new Promise((resolve, reject) => {
      this.http
        .get("applicant/get")
        .then(data => {
          this.applicants = JSON.parse(data.response);
          resolve(this.applicants)
        }).catch(err => reject(err));
    });
    return promise;
  }

  createApplicant(applicant) {
    let promise = new Promise((resolve, reject) => {
      this.http
        .post('applicant/create', applicant)
        .then(data => {
          let newContact = JSON.parse(data.response);
          resolve(newContact);
        }).catch(err => reject(err));
    });
    return promise;
  }

  getApplicant(id) {
    let promise = new Promise((resolve, reject) => {
      this.http
        .get('applicant/get' + id)
        .then(response => {
          let applicant = JSON.parse(response.response);
          resolve(applicant);
        }).catch(err => reject(err))
    });
    return promise;
  }

  deleteApplicant(id) {
    let promise = new Promise((resolve, reject) => {
      this.http
        .delete('applicant/delete' + id)
        .then(delresponse => {
          let response = JSON.parse(delresponse.response);
          resolve(response);
        })
        .catch(err => reject(err));
    });
    return promise;
  }

  updateApplicant(id, applicant) {
    let promise = new Promise((resolve, reject) => {
      this.http
        .put(id, applicant)
        .then(response => {
          let applicantResp = JSON.parse(response.response);
          resolve(applicantResp);
        }).catch(err => reject(err));
    });
    return promise;
  }
}
